#!/usr/bin/ruby

# NIM : 14116145
# License : MIT

class MatriceSq

  attr_accessor :matrices,:msize

  def initialize size
    self.matrices = []
    size.to_i.times do
      self.matrices << [0]
    end
    self.msize = size
  end

  def set_value(row,col,value)
    self.matrices[col][row] = value if ((row < self.msize)&&(col < self.msize))
  end

  def get_value(col,row)
    self.matrices[row][col] if ((row < self.msize)&&(col < self.msize))
  end

  def validate?

    # row check
    self.msize.to_i.times do |iteration_first|
      sum = 0
      self.msize.times do |iteration_second|
        sum +=  self.matrices[iteration_first - 1][iteration_second - 1].to_i
      end
      return false if (sum != 15)
    end

    sum = 0
    puts "Row check pass"

    # column check
    self.msize.to_i.times do |iteration_first|
      sum = 0
      self.msize.times do |iteration_second|
        sum +=  self.matrices[iteration_second - 1][iteration_first - 1].to_i
      end
      return false if (sum != 15)
    end

    sum = 0
    puts "Colum check pass"

    # diagonal check
    self.msize.to_i.times do |iteration|
      sum += self.matrices[iteration - 1][iteration - 1].to_i
    end
    return false if (sum != 15)

    puts "Omedetto"
    # reverse diagonal check
    #self.msize.to_i.times do |iteration|
    #    self.matrices[iteration][iteration]
    #end

    return true
  end

  def show
    self.msize.to_i.times do |iteration_first|
      print "R#{iteration_first} : "
      self.msize.to_i.times do |iteration_second|
        if self.matrices[iteration_first][iteration_second]
           print self.matrices[iteration_first][iteration_second]
        else
          print 0
        end
      end
      puts ""
    end
  end

  def get_empty
    empty_sets = []
    self.msize.to_i.times do |iteration_first|
      self.msize.to_i.times do |iteration_second|
        if(self.matrices[iteration_first][iteration_second].to_i == 0)
          empty_sets << [iteration_first,iteration_second]
        end
      end
    end
    empty_sets
  end

  def get_all_value
    all_sets = []
    self.msize.to_i.times do |iteration_first|
      self.msize.to_i.times do |iteration_second|
        if(self.matrices[iteration_first][iteration_second].to_i > 0)
          all_sets << self.matrices[iteration_first][iteration_second].to_i 
        end
      end
    end
    all_sets
  end

end

sodokaku = MatriceSq.new(3)

# set_value col , row , value
sodokaku.set_value 0,1,9
sodokaku.set_value 2,1,1
sodokaku.set_value 2,0,8

#sodokaku.set_value 2,2,6
#sodokaku.set_value 1,0,3
#sodokaku.set_value 1,1,5

sodokaku.set_value 1,2,7

#sodokaku.set_value 0,0,4
#sodokaku.set_value 0,2,2

test = sodokaku
empty = sodokaku.get_empty
until(test.validate?)
# test = sodokaku
# empty = sodokaku.get_empty
  STDOUT.flush
  inputed_sets = test.get_all_value
  #inputed
  empty.each do |row,col|
    #p " R: #{row} C: #{col}"
    # bikin input random
    begin
      random = false
      input_random = rand(0...9)
       p inputed_sets
      unless(inputed_sets.include? input_random)
        test.set_value col,row,input_random
        inputed_sets << input_random
        random = true
      end
    end while(random)
  end
  test.show
#  system("clear")
end

p sodokaku.validate?

#sum = 0
#matrices = sodokaku.matrices
#sodokaku.msize.times do |itf|
#  sodokaku.msize.times do |its|
#   sum += matrices[itf][its]
#  end
#  p sum
#end

#p sum
p sodokaku.get_empty

sodokaku.show